#include "Renderer.h"
#include "Game.h"
#include "TGUI/TGUI.hpp"

sf::RenderWindow window;
sf::Clock mainClock;
sf::Time prevTime;

Renderer renderer;
Game game;
tgui::Gui gameGui(window), menuGui(window), endGui(window), helpGui(window);
sf::Sprite sundial;
sf::Texture sundial_tex;
sf::Texture end_screen_tex[2];
sf::Sprite end_screen[2];
int scene = 0;
int victor = -1;
const int SCORE_CAP = 1000;

enum {KA_NONE, KA_DOWN, KA_UP, KA_LEFT, KA_RIGHT, KA_COUNT};
bool keyDown[KA_COUNT];

sf::SoundBuffer pressSoundBuffer;
sf::Sound pressSound;

/// return true if success
bool init() {
    sf::ContextSettings settings;
    settings.antialiasingLevel = 8;
    window.create(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Flagfight", sf::Style::Titlebar | sf::Style::Close, settings);
    window.setVerticalSyncEnabled(true);

    renderer.init();

    if (!game.init()) return false;

    for (int i = 0; i < KA_COUNT; i++) {
        keyDown[i] = false;
    }

    gameGui.setGlobalFont("assets/Protoculture.ttf");
    endGui.setGlobalFont(gameGui.getGlobalFont());

    menuGui.loadWidgetsFromFile("assets/main_menu");
    (menuGui.get<tgui::Button>("PlayBtn"))->bindCallback(tgui::Button::LeftMouseClicked);
    (menuGui.get<tgui::Button>("QuitBtn"))->bindCallback(tgui::Button::LeftMouseClicked);
    (menuGui.get<tgui::Button>("HelpBtn"))->bindCallback(tgui::Button::LeftMouseClicked);

    endGui.loadWidgetsFromFile("assets/end_menu");
    (endGui.get<tgui::Button>("RetryBtn"))->bindCallback(tgui::Button::LeftMouseClicked);
    (endGui.get<tgui::Button>("ExitBtn"))->bindCallback(tgui::Button::LeftMouseClicked);

    helpGui.loadWidgetsFromFile("assets/help_menu");
    (helpGui.get<tgui::Button>("ExitBtn"))->bindCallback(tgui::Button::LeftMouseClicked);

    end_screen_tex[0].loadFromFile("assets/screen_victory.png");
    end_screen_tex[1].loadFromFile("assets/screen_defeat.png");
    end_screen[0].setTexture(end_screen_tex[0]);
    end_screen[1].setTexture(end_screen_tex[1]);
    end_screen[0].setPosition(0, 0);
    end_screen[1].setPosition(0, 0);

    sundial_tex.loadFromFile("assets/sundial.png");
    sundial.setTexture(sundial_tex);
    sundial.setPosition(260, 15);
    sundial.setTextureRect(sf::IntRect(0, 0, 96, 96));

    gameGui.loadWidgetsFromFile("assets/game_menu");

    prevTime = mainClock.getElapsedTime();

    pressSoundBuffer.loadFromFile("assets/sounds/press.wav");
    pressSound.setBuffer(pressSoundBuffer);

    return true;
}

/// return true when done
bool events() {
    Player& player = game.getWorld().getPlayer();
    sf::Event event;
    while(window.pollEvent(event)) {
        if(scene == 0){
            menuGui.handleEvent(event);
        }else if(scene == 1 && victor == -1){
            gameGui.handleEvent(event);
        }else if(scene == 1 && victor > -1){
            endGui.handleEvent(event);
        }else if(scene == 2){
            helpGui.handleEvent(event);
        }
        switch(event.type) {
            case sf::Event::Closed: return true;
            case sf::Event::MouseButtonPressed: {
                if (scene == 1 && victor == -1 && player.getAmmunition() > 0 && player.getStun() <= 0) {
                    game.getWorld().getPlayer().fireGun();
                }
            } break;
            case sf::Event::KeyPressed: {
                if(scene == 1 && victor == -1){
                    switch(event.key.code) {
                        case (sf::Keyboard::W):
                        case (sf::Keyboard::Up):
                            keyDown[KA_UP] = true; break;
                        case (sf::Keyboard::A):
                        case (sf::Keyboard::Left):
                            keyDown[KA_LEFT] = true; break;
                        case (sf::Keyboard::S):
                        case (sf::Keyboard::Down):
                            keyDown[KA_DOWN] = true; break;
                        case (sf::Keyboard::D):
                        case (sf::Keyboard::Right):
                            keyDown[KA_RIGHT] = true; break;
                        case (sf::Keyboard::Space):
                        case (sf::Keyboard::E):
                            game.getWorld().getPlayer().pickUpFlag(); break;
                        //case sf::Keyboard::R:
                         //   scene = 0;
                         //   game.getWorld().denit();
                         //   break;
                        default: break;
                    }
                }
            } break;
            case sf::Event::KeyReleased: {
                if(scene == 1 && victor == -1){
                    switch(event.key.code) {
                        case (sf::Keyboard::W):
                        case (sf::Keyboard::Up):
                            keyDown[KA_UP] = false; break;
                        case (sf::Keyboard::A):
                        case (sf::Keyboard::Left):
                            keyDown[KA_LEFT] = false; break;
                        case (sf::Keyboard::S):
                        case (sf::Keyboard::Down):
                            keyDown[KA_DOWN] = false; break;
                        case (sf::Keyboard::D):
                        case (sf::Keyboard::Right):
                            keyDown[KA_RIGHT] = false; break;
                        default: break;
                    }
                }
            }
            default: break;
        }
    }

    tgui::Callback callback;
    while(menuGui.pollCallback(callback)){
        if(callback.id == 0){
            pressSound.play();
            game.getWorld().init();
            for (int i = 0; i < KA_COUNT; i++) {
                keyDown[i] = false;
            }
            victor = -1;
            scene = 1;
        }else if(callback.id == 1){
            pressSound.play();
            // Quit button
            return true;
        }else if(callback.id == 2){
            pressSound.play();
            // Help button
            scene = 2;
        }
    }
    while(endGui.pollCallback(callback)){
        if(callback.id == 0){
            pressSound.play();
            // Retry button
            game.getWorld().denit();
            game.getWorld().init();
            for (int i = 0; i < KA_COUNT; i++) {
                keyDown[i] = false;
            }
            victor = -1;
        }else if(callback.id == 1){
            pressSound.play();
            // Exit button
            game.getWorld().denit();
            scene = 0;
        }
    }
    while(helpGui.pollCallback(callback)){
        if(callback.id == 0){
            // Exit button
            scene = 0;
        }
    }

    if(scene == 1 && victor == -1) {
        if (player.getStun() <= 0) {
            int dir = NONE;
            if (keyDown[KA_UP]) dir |= NORTH;
            else if (keyDown[KA_DOWN]) dir |= SOUTH;
            if (keyDown[KA_LEFT]) dir |= WEST;
            else if (keyDown[KA_RIGHT]) dir |= EAST;
            player.move(dir, 1.5);
        }

        sf::Vector2i mousePos = sf::Mouse::getPosition(window);
        double angle = std::atan2(mousePos.x - 40 - WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2 - mousePos.y);
        player.setAngle(angle);
    }

    return false;
}

void render() {
    window.clear();
    if(scene == 1){
        renderer.render(window, game);
        window.draw(sundial);
        gameGui.draw();
        renderer.renderNext(window, game);
        if(victor == 0){
            window.draw(end_screen[0]);
        }else if(victor > 0){
            window.draw(end_screen[1]);
        }
        if(victor != -1){
            char buffer[128];
            sprintf(buffer, "Player 1 score: %d", game.getWorld().getPlayer().getScore());
            (endGui.get<tgui::Label>("p1_score"))->setText(buffer);

            sprintf(buffer, "Player 2 score: %d", game.getWorld().getEnemy(0).getScore());
            (endGui.get<tgui::Label>("p2_score"))->setText(buffer);

            sprintf(buffer, "Player 3 score: %d", game.getWorld().getEnemy(1).getScore());
            (endGui.get<tgui::Label>("p3_score"))->setText(buffer);

            sprintf(buffer, "Player 4 score: %d", game.getWorld().getEnemy(2).getScore());
            (endGui.get<tgui::Label>("p4_score"))->setText(buffer);

            endGui.draw();
        }
    } else if(scene == 0) {
        menuGui.draw();
    } else if(scene == 2){
        helpGui.draw();
    }
    window.display();
}

void formatScore(const char* label, int score, float x){
    char buffer[16];
    sprintf(buffer, "%d", score);
    tgui::Label::Ptr lbl = gameGui.get<tgui::Label>(label);
    lbl->setText(buffer);

    sf::Text sfStr;
    sfStr.setString(buffer);
    sfStr.setFont(gameGui.getGlobalFont());
    sfStr.setCharacterSize(30);
    sf::FloatRect sz = sfStr.getLocalBounds();

    lbl->setPosition(x - 0.5f * sz.width, 19.0f);
}

void logic(sf::Time diff) {
    float difff = diff.asMicroseconds() / 1000000.f;
    game.getWorld().getPlayer().getMap()->update(difff);
    game.getWorld().update(difff);

    if(game.getWorld().getPlayer().getScore() >= SCORE_CAP){
        victor = 0;
        return;
    }
    for(int i = 0; i < 3; i++){
        if(game.getWorld().getEnemy(i).getScore() >= SCORE_CAP){
            victor = i + 1;
            return;
        }
    }

    formatScore("p1_score", game.getWorld().getPlayer().getScore(), 398.0f + 0.5f * 100.0f);
    formatScore("p2_score", game.getWorld().getEnemy(0).getScore(), 398.0f + 1.5f * 100.0f);
    formatScore("p3_score", game.getWorld().getEnemy(1).getScore(), 398.0f + 2.5f * 100.0f);
    formatScore("p4_score", game.getWorld().getEnemy(2).getScore(), 398.0f + 3.5f * 100.0f);

    int pos = ((int)(game.getWorld().getTimeOfDay() * 12.0) + 9) % 12;
    sundial.setTextureRect(sf::IntRect(96 * pos, 0, 96, 96));
}

/// return true when done
bool execute() {
    sf::Time newTime = mainClock.getElapsedTime();
    sf::Time diff = newTime - prevTime;
    prevTime = newTime;

    if (events()) return true;
    if(scene == 1 && victor == -1){
        logic(diff);
    }
    render();
    return false;
}

void clean() {
    window.close();
}

int main(int argc, char** argv) {
    if (init()) while(!execute());
    else return EXIT_FAILURE;
    clean();
    return EXIT_SUCCESS;
}
