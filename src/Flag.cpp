#include "Flag.h"

Flag::Flag(sf::Vector2f location, sf::Texture& tex): Entity(sf::FloatRect(location.x, location.y, 64, 64)) {
    sprite.setTexture(tex);
    held = false;
    scored = false;
    points = 30;
    next = 0.f;
}

Flag::~Flag() {
    //dtor
}

bool Flag::update(float diff) {
    return false;
}

int Flag::depoint(float diff) {
    next += diff;
    int p = 0;
    while(next > .1f) {
        p++;
        next -= .1f;
    }
    if (points < p) p = points;
    points -= p;
    return p;
}
void Flag::hold() {
    held = true;
}
void Flag::drop() {
    held = false;
}
void Flag::score() {
    scored = true;
    held = false;
}
bool Flag::isScored() {
    return scored;
}
bool Flag::isHeld() {
    return held;
}

sf::Vector2f Flag::getCenterPosition() {
    return getPosition() + sf::Vector2f(32, 32);
}
