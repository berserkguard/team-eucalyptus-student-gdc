#include "Entity.h"

Entity::Entity() { }
Entity::Entity(sf::FloatRect bounds): bounds(bounds) {
    sprite.setPosition(bounds.left, bounds.top);
}
Entity::Entity(sf::Vector2f position): bounds(position, sf::Vector2f(0, 0)) {
    sprite.setPosition(position);
}
Entity::Entity(sf::Sprite sprite): sprite(sprite), bounds(sprite.getGlobalBounds()) { }

Entity::~Entity() {}

sf::Sprite& Entity::getSprite() {
    return sprite;
}
sf::FloatRect& Entity::getCollisionBox() {
    return bounds;
}

void Entity::setPosition(sf::Vector2f pos) {
    translate(pos.x - bounds.left, pos.y - bounds.top);
}
void Entity::setPosition(float x, float y) {
    translate(x - bounds.left, y - bounds.top);
}
const sf::Vector2f Entity::getPosition() {
    return sf::Vector2f(bounds.left, bounds.top);
}
void Entity::translate(sf::Vector2f disp) {
    sprite.move(disp);
    bounds.left += disp.x;
    bounds.top += disp.y;
}
void Entity::translate(float x, float y) {
    sprite.move(x, y);
    bounds.left += x;
    bounds.top += y;
}

void Entity::render(sf::RenderWindow& window){
    window.draw(sprite);
}
