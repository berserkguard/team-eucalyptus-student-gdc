/*
 *  Copyright 2013 Luke Puchner-Hardman
 *
 *  This file is part of Hoarder.
 *  Hoarder is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hoarder is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Hoarder.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Rand.h"

#include <SFML/System.hpp>

std::mt19937 Rand::gen = std::mt19937(12);

unsigned Rand::simple(unsigned high) {
    std::uniform_int_distribution<int> die(0, high - 1);
    return die(gen);
}

int Rand::i(int low, int high) {
    std::uniform_int_distribution<int> die(low, high - 1);
    return die(gen);
}

float Rand::f(float low, float high) {
    std::uniform_real_distribution<float> die(low, high);
    return die(gen);
}

double Rand::d(double low, double high) {
    std::uniform_real_distribution<double> die(low, high);
    return die(gen);
}

double Rand::norm(double mean, double standardDeviation) {
    std::normal_distribution<double> die(mean, standardDeviation);
    return die(gen);
}

double Rand::lognorm(double mean, double standardDeviation) {
    std::lognormal_distribution<double> die(mean, standardDeviation);
    return die(gen);
}

double Rand::exp(double lambda) {
    std::exponential_distribution<double> die(lambda);
    return die(gen);
}

int Rand::dice(int sides, int num) {
    std::uniform_int_distribution<int> die(1, sides);
    int sum = 0;
    for (int i = 0; i < sum; i++) {
        sum += die(gen);
    }
    return sum;
}

const int Rand::RANDA = 14741;
const int Rand::RANDB = 7919;
const int Rand::RANDC = 44318;
const int Rand::RANDM = rand();
const int Rand::RANDN = rand();
int Rand::consistent(int low, int high, int a, int b, int c) {
    unsigned short r = (RANDA * a + RANDB * b + RANDC * c + RANDM) ^ RANDN;
    return r % (high - low) + low;
}
