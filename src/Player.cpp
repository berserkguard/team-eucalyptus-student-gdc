#include "Player.h"
#include "BulletEntity.h"

Player::Player(): Mob(PLAYER) {
    currentMap = NULL;
    flag = NULL;
    index = 0;
    score = 0;
    angle = 0;

    stun = 0;
    stunAngle = 0;

    ai.state = WAITING;
    ai.path = NULL;
    ai.pointOnPath = 0;
    ai.nearFlagged = NULL;
    ai.cooldown = 0;
    ai.choiceCooldown = 0;

    deadInLava = false;

    ammunition = 90;

    angry = 0;

    bullettex.loadFromFile("assets/projectile.png");
}

Player::~Player() { }

void Player::reset() {
    flag = NULL;
    index = 0;
    score = 0;
    angle = 0;

    stun = 0.f;
    stunAngle = 0;

    ai.state = WAITING;
    ai.path = NULL;
    ai.pointOnPath = 0;
    ai.nearFlagged = NULL;
    ai.cooldown = 0;
    ai.choiceCooldown = 0;

    deadInLava = false;

    angry = 0;

    ammunition = 90;
}

bool toRotateClockwise(double from, double to) {
    float direction = cos(from) * sin(to) - cos(to) * sin(from);
    if (direction > 0.0f) {
        return true;
    } else {
        return false;
    }
}

float Player::getVolume() {
    if (index == 0) return 100;
    auto& mobs = currentMap->getMobs();
    for (auto iter = mobs.begin(); iter != mobs.end(); ++iter) {
        Mob* mob = *iter;
        if (mob->getType() == PLAYER && mob != this) {
            Player* player = (Player*)mob;
            if (player->getIndex() == 0) {
                sf::Vector2f diff = player->getCenterPosition() - getCenterPosition();
                float dist = std::sqrt(diff.x * diff.x + diff.y * diff.y);
                if (dist >= 1000) return 0;
                return (1000 - dist) / 10;
            }
        }
    }
    return 0;
}

void Player::goToward(sf::Vector2f where, float diff) {
    sf::Vector2f pos = getCenterPosition();
    double newAngle = std::atan2(pos.x - where.x, where.y - pos.y);
    newAngle += 3.14159 / 2;
    if (!ai.nearFlagged) {
        double change = diff * 4;
        if (std::abs(angle - newAngle) < change || std::abs(angle - newAngle - 2 * 3.14159) < change || std::abs(angle - newAngle + 2 * 3.14159) < change) {
            angle = newAngle;
        } else if (toRotateClockwise(angle, newAngle)) {
            angle += change;
        } else {
            angle -= change;
        }
        setAngle(angle);
    }

    // YELLOW POWER
    if (index == 3 && flag) {
        moveAngle(newAngle, 2);
    } else {
        moveAngle(newAngle, 1.5);
    }
}

void Player::initSounds(SoundBuffers& soundBuffers) {
    for (int i = 0; i < 3; i++) sounds.shoot[i].setBuffer(soundBuffers.shoot[i]);
    sounds.hit.setBuffer(soundBuffers.hit);
    sounds.pickup.setBuffer(soundBuffers.pickup);
    sounds.drop.setBuffer(soundBuffers.drop);
    sounds.score.setBuffer(soundBuffers.score);
    sounds.ammo.setBuffer(soundBuffers.ammo);
}

void Player::followPath(float diff) {
    sf::Vector2i pos = sf::Vector2i(getCenterPosition() * (1 / 64.f));
    if (ai.pointOnPath >= ai.path->length()) {
        goToward(ai.currentTarget, diff);
        rethink();
        return;
    }
    sf::Vector2i next = ai.path->at(ai.pointOnPath);
    if (pos == next) ai.pointOnPath++;
    else goToward(sf::Vector2f(next * TILE_SIZE + sf::Vector2i(32, 32)), diff);
}

Player* Player::getNearFlagPlayer(bool forReal) {
    Player* nearest = NULL;
    double nearestSquareDist = 1000000000.f;
    auto& mobs = currentMap->getMobs();
    for (auto iter = mobs.begin(); iter != mobs.end(); ++iter) {
        Mob* mob = *iter;
        if (mob->getType() == PLAYER && mob != this) {
            Player* player = (Player*)mob;
            if (!player->getDeadInLava() && (player->hasFlag() || !forReal)) {
                sf::Vector2f diff = player->getCenterPosition() - getCenterPosition();
                double squareDist = diff.x * diff.x + diff.y * diff.y;
                if (squareDist < 1000 * 1000 && squareDist < nearestSquareDist) {
                    nearestSquareDist = squareDist;
                    nearest = player;
                }
            }
        }
    }
    return nearest;
}

void Player::rethink() {
    ai.state = WAITING;
    delete ai.path;
    ai.path = NULL;
    ai.pointOnPath = 0;
}

bool Player::update(float diff) {
    sf::Vector2f vel = getVelocity();
    if (currentMap->isSlippery()) {
        vel.x *= .95f;
        vel.y *= .95f;
        translate(std::max(std::min(vel.x, (float)TERMINAL_VELOCITY), -(float)TERMINAL_VELOCITY) * .5f,
              std::max(std::min(vel.y, (float)TERMINAL_VELOCITY), -(float)TERMINAL_VELOCITY) * .5f);
    } else {
        vel.x *= .75f;
        vel.y *= .75f;
        translate(std::max(std::min(vel.x, (float)TERMINAL_VELOCITY), -(float)TERMINAL_VELOCITY),
              std::max(std::min(vel.y, (float)TERMINAL_VELOCITY), -(float)TERMINAL_VELOCITY));
    }

    setVelocity(vel);

    if (deadInLava) {
        stun -= diff;
        if (stun <= 0.f) {
            auto& startingPositions = currentMap->getStartingPositions();
            setPosition(sf::Vector2f(startingPositions[Rand::i(0, startingPositions.size())] * TILE_SIZE));
            deadInLava = false;
        }
        return false;
    }

    if (flag) {
        score += flag->depoint(diff);
    }

    sf::Vector2i pos = sf::Vector2i(getCenterPosition() * (1 / 64.f));
    Tile* tile = &currentMap->getTile(pos);
    if (tile->isLava()) {
        stun = 5.f;
        deadInLava = true;
        currentMap->scoreFlag(flag);
        flag = NULL;
        addScore(-50);
        return false;
    }
    while (stun > 0 && !tile->isPassable()) {
        if (pos.x > currentMap->getSize().x / 2) {
            translate(-TILE_SIZE, 0);
        } else {
            translate(TILE_SIZE, 0);
        }
        if (pos.y > currentMap->getSize().y / 2) {
            translate(0, -TILE_SIZE);
        } else {
            translate(0, TILE_SIZE);
        }
        pos = sf::Vector2i(getCenterPosition() * (1 / 64.f));
        if (pos.x >= currentMap->getSize().x || pos.x < 0 || pos.y >= currentMap->getSize().y || pos.y < 0) {
            auto& startingPositions = currentMap->getStartingPositions();
            setPosition(sf::Vector2f(startingPositions[Rand::i(0, startingPositions.size())] * TILE_SIZE));
            return false;
        }
        tile = &currentMap->getTile(pos);
    }

    stun -= diff;
    //if (stun < 0) stun = 0;
    if (stun > 0) {
        moveAngle(stunAngle, stun);
        return false;
    }

    // if an AI
    if (index) {
        angry -= diff;
        if (ai.state == GETTING_FLAG) {
            if (angry <= 0) ai.nearFlagged = getNearFlagPlayer();
            if (ai.targetFlag->isHeld() || ai.targetFlag->isScored()) {
                rethink();
            } else if (pickUpFlag()) {
                rethink();
            } else {
                followPath(diff);
            }
        } else if (ai.state == RETURNING) {
            if (!hasFlag()) {
                rethink();
            } else if (tile->isGoal()) {
                pickUpFlag();
                rethink();
            } else {
                followPath(diff);
            }
        } else if (ai.state == WAITING) {
            if (angry <= 0) ai.nearFlagged = getNearFlagPlayer();
            if (hasFlag()) {
                ai.path = new Path(pos, currentMap->getGoal(), currentMap);
                ai.path->generate();
                ai.pointOnPath = 0;
                ai.currentTarget = sf::Vector2f(currentMap->getGoal() * 64);
                ai.state = RETURNING;
            } else if (!ammunition && !currentMap->getAmmos().empty()) {
                auto& ammos = currentMap->getAmmos();
                Path* minPath = NULL;
                sf::Vector2i minAmmo;
                for (auto iter = ammos.begin(); iter != ammos.end(); ++iter) {
                    sf::Vector2i ammoLoc = *iter;
                    Path* path = new Path(pos, ammoLoc, currentMap);
                    path->generate();
                    if (!minPath || minPath->distance() > path->distance()) {
                        delete minPath;
                        minPath = path;
                        minAmmo = ammoLoc;
                    } else {
                        delete path;
                    }
                }
                if (minPath) {
                    ai.path = minPath;
                    ai.pointOnPath = 0;
                    ai.currentTarget = sf::Vector2f(minAmmo * 64);
                    ai.state = GETTING_AMMO;
                }
            } else {
                auto& flags = currentMap->getFlags();
                if (!flags.empty()) {
                    Path* minPath = NULL;
                    Flag* minFlag = NULL;
                    for (auto iter = flags.begin(); iter != flags.end(); ++iter) {
                        Flag* flag = *iter;
                        Path* path = new Path(pos, sf::Vector2i(flag->getCenterPosition() / (float)TILE_SIZE), currentMap);
                        path->generate();
                        if (!minPath || minPath->distance() > path->distance()) {
                            delete minPath;
                            minPath = path;
                            minFlag = flag;
                        } else {
                            delete path;
                        }
                    }
                    if (minPath && minFlag) {
                        ai.path = minPath;
                        ai.pointOnPath = 0;
                        ai.currentTarget = minFlag->getCenterPosition();
                        ai.state = GETTING_FLAG;
                        ai.targetFlag = minFlag;
                    }
                } else if (ai.nearFlagged) {
                    ai.path = new Path(pos, sf::Vector2i(ai.nearFlagged->getCenterPosition() * (1 / 64.f)), currentMap);
                    ai.path->generate();
                    ai.pointOnPath = 0;
                    ai.currentTarget = sf::Vector2f(currentMap->getGoal() * 64);
                    ai.state = CHASING;
                } else if (/*night*/true) {
                    if (Rand::i(0, 2)) {
                        ai.nearFlagged = getNearFlagPlayer(false);
                        for (int i = 0; i < 3; i++) {
                            int x = Rand::i(0, currentMap->getSize().x);
                            int y = Rand::i(0, currentMap->getSize().y);
                            Tile& tile = currentMap->getTile(x, y);
                            if (tile.isPassable()) {
                                ai.path = new Path(pos, sf::Vector2i(x, y), currentMap);
                                ai.path->generate();
                                ai.pointOnPath = 0;
                                ai.choiceCooldown = 6.f;
                                ai.state = HUNTING;
                                break;
                            }
                        }
                    } else {
                        ai.nearFlagged = getNearFlagPlayer(false);
                        if (ai.nearFlagged) {
                            ai.path = new Path(pos, sf::Vector2i(ai.nearFlagged->getCenterPosition() * (1 / 64.f)), currentMap);
                            ai.path->generate();
                            ai.choiceCooldown = 5.f;
                            ai.pointOnPath = 0;
                            ai.state = HUNTING;
                        }
                    }
                }
            }
        } else if (ai.state == CHASING) {
            if (!ai.nearFlagged->hasFlag()) {
                rethink();
            } else {
                followPath(diff);
            }
        } else if (ai.state == HUNTING) {
            ai.choiceCooldown -= diff;
            if (ai.choiceCooldown <= 0 || !night || !ammunition || (ai.nearFlagged && ai.nearFlagged->getDeadInLava())) {
                rethink();
            } else {
                followPath(diff);
            }
        } else if (ai.state == GETTING_AMMO) {
            Tile& destTile = currentMap->getTile(sf::Vector2i(ai.currentTarget  * ( 1 / 64.f)));
            if (!destTile.hasAmmunition()) {
                rethink();
            } else if (pickUpFlag()) {
                rethink();
            } else {
                followPath(diff);
            }
        }
        if (ai.nearFlagged) {
            double change = diff * 4;
            sf::Vector2f posDiff = ai.nearFlagged->getCenterPosition() - getCenterPosition();
            double pAngle = atan2(posDiff.x, -posDiff.y);
            if (std::abs(angle - pAngle) < change || std::abs(angle - pAngle - 2 * 3.14159) < change || std::abs(angle - pAngle + 2 * 3.14159) < change) {
                angle = pAngle;
            } else if (toRotateClockwise(angle, pAngle)) {
                angle += change;
            } else {
                angle -= change;
            }
            ai.cooldown -= diff;
            if (ai.cooldown <= 0) {
                fireGun();

                // GREEN POWER
                if (index == 2) {
                    ai.cooldown = .14f;
                } else {
                    ai.cooldown = .20f;
                }
                if (ammunition <= 0) ai.cooldown *= 2;

                if (currentMap->getDifficulty() == 0) ai.cooldown *= 1.5f;
                else if (currentMap->getDifficulty() == 2) ai.cooldown *= .7f;
            }
        }
    }
    return false;
}

Map* Player::getMap() {
    return currentMap;
}

void Player::setMap(Map* map) {
    currentMap = map;
}

void Player::fireGun() {
    if(flag) {
        return;
    }
    ammunition--;
    if (ammunition < 0) ammunition = 0;

    double bulletAngle = angle;
    if (index) {
        bulletAngle += Rand::f(-.1, .1);
    }

    sf::Vector2f velocity(700.0f * cos(bulletAngle - 3.1415926 / 2.0), 700.0f * sin(bulletAngle - 3.1415926 / 2.0));
    sf::Vector2f bullet_pos = this->getCenterPosition();
    float xnew = 48.0f * cos(bulletAngle - 3.1415926 / 2.0) - 16.0f * sin(bulletAngle - 3.1415926 / 2.0);
    float ynew = 48.0f * sin(bulletAngle - 3.1415926 / 2.0) + 16.0f * cos(bulletAngle - 3.1415926 / 2.0);
    bullet_pos.x += xnew - 8.0f;
    bullet_pos.y += ynew - 8.0f;
    BulletEntity* bullet = new BulletEntity(bullet_pos, velocity, this, bullettex);

    currentMap->addEntity(bullet);

    int i = Rand::i(0, 3);
    sounds.shoot[i].setVolume(getVolume());
    sounds.shoot[i].play();
}

void Player::getHitByGun(double angle, Player* who) {
    if (hasFlag()) {
        pickUpFlag();
    }
    stunAngle = angle;

    // RED POWER
    if (night || index == 1) {
        stun = .1f;
        moveAngle(angle, 20);
    } else {
        stun = .3f;
        moveAngle(angle, 40);
    }
    sounds.hit.setVolume(getVolume());
    sounds.hit.play();

    if (who && index) {
        ai.nearFlagged = who; //provocation
        angry = 2;
    }
}

bool Player::pickUpFlag() {
    sf::Vector2i pos = sf::Vector2i(getCenterPosition() * (1 / 64.f));
    Tile& tile = currentMap->getTile(pos);
    auto& flags = currentMap->getFlags();

    if (tile.hasAmmunition()) {
        tile.setAmmunition(false);
        ammunition += 50;
        if (ammunition > 90) ammunition = 90;
        sounds.ammo.setVolume(getVolume());
        sounds.ammo.play();
        auto& ammos = currentMap->getAmmos();
        ammos.erase(ammos.find(pos));
        return true;
    } else if (flag) {
        if (tile.isGoal()) {
            score += FLAG_POINTS;
            flag->score();
            sounds.score.setVolume(getVolume());
            sounds.score.play();
            currentMap->scoreFlag(flag);
        } else {
            flags.insert(flag);
            sounds.drop.setVolume(getVolume());
            sounds.drop.play();
            flag->drop();
            flag->setPosition(getPosition() - sf::Vector2f(12, 12));
        }
        flag = NULL;
    } else {
        for (auto iter = flags.begin(); iter != flags.end(); ++iter) {
            Flag* curFlag = *iter;
            if (curFlag->getCollisionBox().contains(getCenterPosition())) {
                flag = curFlag;
                flag->hold();
                sounds.pickup.setVolume(getVolume());
                sounds.pickup.play();
                flags.erase(iter);
                return true;
            }
        }
    }
    return false;
}

int Player::getScore() {
    return score;
}

void Player::addScore(int s) {
    score += s;
    if (score < 0) score = 0;
}

void Player::setAngle(double a) {
    angle = a;
}
double Player::getAngle() {
    return angle;
}

bool Player::shouldCollide() {
    return !index || stun > -1.f;
}

bool Player::hasFlag() {
    return flag;
}

void Player::setIndex(int i) {
    index = i;
    if (index) stun = 2.f;
}
int Player::getIndex() {
    return index;
}

float Player::getStun() {
    return stun;
}

bool Player::isNight() {
    return night;
}
void Player::setNight(int n) {
    night = n;
}

int Player::getAmmunition() {
    return ammunition;
}

bool Player::getDeadInLava() {
    return deadInLava;
}
