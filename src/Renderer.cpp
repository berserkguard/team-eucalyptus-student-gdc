#include "Renderer.h"

Renderer::Renderer() {
    //ctor
}

Renderer::~Renderer() {
    //dtor
}

void Renderer::init() {
    tileset.loadFromFile("assets/tileset.png");
    minitileset.loadFromFile("assets/minitileset.png");
    ammotex.loadFromFile("assets/bullet_crate.png");
    ammohave.loadFromFile("assets/bullet.png");
    ammoempty.loadFromFile("assets/bullet_empty.png");

    playerGun[0].loadFromFile("assets/players/player_1.png");
    playerGun[1].loadFromFile("assets/players/player_2.png");
    playerGun[2].loadFromFile("assets/players/player_3.png");
    playerGun[3].loadFromFile("assets/players/player_4.png");
    playerFlag[0].loadFromFile("assets/players/player_1_flag.png");
    playerFlag[1].loadFromFile("assets/players/player_2_flag.png");
    playerFlag[2].loadFromFile("assets/players/player_3_flag.png");
    playerFlag[3].loadFromFile("assets/players/player_4_flag.png");
    playerColors[0] = sf::Color::Blue;
    playerColors[1] = sf::Color::Red;
    playerColors[2] = sf::Color::Green;
    playerColors[3] = sf::Color::Yellow;
}

void Renderer::render(sf::RenderWindow& window, Game& game) {
    sf::View view = window.getDefaultView();
    view.move(view.getSize().x * -.5, view.getSize().y * -.5);
    view.move(sf::Vector2f(sf::Vector2i(game.getWorld().getPlayer().getPosition())));
    window.setView(view);

    const sf::Vector2f vs = window.getView().getSize();
    const sf::Vector2f vp = window.getView().getCenter() - sf::Vector2f(vs.x / 2, vs.y / 2);

    Map* currentMap = game.getWorld().getPlayer().getMap();
    const sf::Vector2i& size = currentMap->getSize();

    for (int y = 0; y < size.y; y++) {
        if (y + 1 < vp.y / TILE_SIZE || y > (vp.y + vs.y) / TILE_SIZE) continue;
        for (int x = 0; x < size.x; x++) {
            if (x + 1 < vp.x / TILE_SIZE || x > (vp.x + vs.x) / TILE_SIZE) continue;

            sf::Vector2i pos(x, y);
            Tile& tile = currentMap->getTile(x, y);

            // make and draw a sprite for each layer in this tile
            for (int j = 0; j < NUM_TILE_LAYERS; j++) {
                short type = tile.getType(j);
                if (type == -1) continue;
                sf::Vector2i t(type % 8, type / 8);
                sf::Sprite tileSprite(tileset, sf::IntRect(TILE_SIZE * t, sf::Vector2i(64, 64)));
                tileSprite.setPosition(sf::Vector2f(pos * TILE_SIZE));
                window.draw(tileSprite);
            }

            if (tile.hasAmmunition()) {
                sf::Sprite ammoSprite(ammotex);
                ammoSprite.setPosition(x * TILE_SIZE, y * TILE_SIZE);
                window.draw(ammoSprite);
            }

            // debug wall drawing
            /*if (tile.hasNorthWall()) {
                sf::RectangleShape line(sf::Vector2f(TILE_SIZE, 10));
                line.setPosition(x * TILE_SIZE, y * TILE_SIZE - 5);
                line.setFillColor(sf::Color::Green);
                window.draw(line);
            }
            if (tile.hasWestWall()) {
                sf::RectangleShape line(sf::Vector2f(10, TILE_SIZE));
                line.setPosition(x * TILE_SIZE - 5, y * TILE_SIZE);
                line.setFillColor(sf::Color::Green);
                window.draw(line);
            }*/
        }
    }

    auto& flags = currentMap->getFlags();
    for (auto iter = flags.begin(); iter != flags.end(); ++iter) {
        Flag* flag = *iter;
        flag->render(window);
    }

    // iterate through all the mobs in the current map
    const std::set<Mob*>& mobs = currentMap->getMobs();
    for (auto iter = mobs.begin(); iter != mobs.end(); ++iter) {
        Mob* mob = *iter;
        if (mob->getType() == PLAYER) {
            Player* player = (Player*)mob;
            sf::Sprite playerSprite;
            if (player->hasFlag()) {
                playerSprite.setTexture(playerFlag[player->getIndex()]);
            } else {
                playerSprite.setTexture(playerGun[player->getIndex()]);
            }
            playerSprite.setOrigin(sf::Vector2f(32, 49));
            playerSprite.setPosition(player->getCenterPosition());
            playerSprite.setRotation(player->getAngle() / 3.14159 * 180);

            if (player->getDeadInLava()) {
                playerSprite.setColor(sf::Color(127, 0, 0));
            }

            window.draw(playerSprite);
        } else {
            sf::FloatRect& bb = mob->getCollisionBox();
            sf::RectangleShape rect(sf::Vector2f((int)bb.width, (int)bb.height));
            rect.setPosition((int)bb.left, (int)bb.top);
            rect.setFillColor(sf::Color::Blue);
            window.draw(rect);
        }
    }

    // Iterate through all the entities in the current map
    const std::set<Entity*>& entities = currentMap->getEntities();
    for (auto iter = entities.begin(); iter != entities.end(); ++iter) {
        Entity* ent = *iter;
        ent->render(window);
    }

    window.setView(window.getDefaultView());

    // darkness
    double nd;
    double time = game.getWorld().getTimeOfDay();
    if (time < .1f) {
        nd = time * 10;
    } else if (time < .6f) {
        nd = 1;
    } else if (time < .7f) {
        nd = (.7 - time) * 10;
    } else {
        nd = 0;
    }
    int n = 255 - (nd * 180 + 75);
    sf::RectangleShape darkness(sf::Vector2f(WINDOW_WIDTH, WINDOW_HEIGHT));
    darkness.setFillColor(sf::Color(0, 0, 0, n));
    window.draw(darkness);

    //minimap
    for (int y = 0; y < size.y; y++) {
        for (int x = 0; x < size.x; x++) {
            sf::Vector2i pos(x, y);
            Tile& tile = currentMap->getTile(x, y);

            // make and draw a sprite for each layer in this tile
            for (int j = 0; j < NUM_TILE_LAYERS; j++) {
                short type = tile.getType(j);
                if (type == -1) continue;
                sf::Vector2i t(type % 8, type / 8);
                sf::Sprite tileSprite(minitileset, sf::IntRect(MTILE_SIZE * t, sf::Vector2i(MTILE_SIZE, MTILE_SIZE)));
                tileSprite.setPosition(sf::Vector2f(pos * MTILE_SIZE));
                if (nd == 0) tileSprite.setColor(sf::Color(127, 127, 127));
                window.draw(tileSprite);
            }

            if (nd > .2 && tile.hasAmmunition()) {
                sf::Sprite tileSprite(minitileset, sf::IntRect(sf::Vector2i(6, 7) * MTILE_SIZE, sf::Vector2i(MTILE_SIZE, MTILE_SIZE)));
                tileSprite.setPosition(sf::Vector2f(pos * MTILE_SIZE));
                window.draw(tileSprite);
            }
        }
    }

    if (nd > .2) {
        for (auto iter = mobs.begin(); iter != mobs.end(); ++iter) {
            Mob* mob = *iter;
            if (mob->getType() == PLAYER) {
                Player* player = (Player*)mob;
                sf::CircleShape circle(2);
                circle.setPosition(player->getCenterPosition() * ((float)MTILE_SIZE / TILE_SIZE) - sf::Vector2f(2, 2));
                circle.setFillColor(playerColors[player->getIndex()]);
                window.draw(circle);
            }
        }

        for (auto iter = flags.begin(); iter != flags.end(); ++iter) {
            Flag* flag = *iter;
            sf::Sprite tileSprite(minitileset, sf::IntRect(sf::Vector2i(7, 7) * MTILE_SIZE, sf::Vector2i(MTILE_SIZE, MTILE_SIZE)));
            tileSprite.setPosition(flag->getPosition() * ((float)MTILE_SIZE / TILE_SIZE));
            window.draw(tileSprite);
        }
    }
}
void Renderer::renderNext(sf::RenderWindow& window, Game& game) {
    int ammo = game.getWorld().getPlayer().getAmmunition();
    sf::Sprite ammoSprite;
    for (int i = 0; i < 15; i++) {
        if (ammo > i * 6) {
            ammoSprite.setTexture(ammohave);
        } else {
            ammoSprite.setTexture(ammoempty);
        }
        ammoSprite.setPosition(370 + i * (ammohave.getSize().x - 1), 47);
        window.draw(ammoSprite);
    }
}
