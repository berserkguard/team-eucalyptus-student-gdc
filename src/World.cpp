#include "World.h"

World::World() {
    player.initSounds(soundBuffers);
    for (int i = 0; i < 3; i++) {
        enemies[i].initSounds(soundBuffers);
    }
    time = 0;
    night = false;
    map = NULL;
}

World::~World() {
    delete map;
}

void World::init() {
    map = new Map("Battlefield", sf::Vector2i(64, 64));
    map->loadMapFromTiled("map.tmx");

    player.reset();
    player.setMap(map);
    map->addMob(&player);

    auto startingPositions = map->getStartingPositions();
    std::cout << startingPositions.size() << std::endl;

    sf::FloatRect& rect = player.getCollisionBox();
    rect.width = 40;
    rect.height = 40;
    int i = Rand::i(0, startingPositions.size());
    player.setPosition(sf::Vector2f(startingPositions[i] * TILE_SIZE));
    startingPositions.erase(startingPositions.begin() + i);
    player.setIndex(0);

    for (int j = 0; j < 3; j++) {
        enemies[j].reset();
        enemies[j].setMap(map);
        map->addMob(&enemies[j]);
        sf::FloatRect& rect = enemies[j].getCollisionBox();
        rect.width = 40;
        rect.height = 40;
        i = Rand::i(0, startingPositions.size());
        enemies[j].setPosition(sf::Vector2f(startingPositions[i] * TILE_SIZE));
        startingPositions.erase(startingPositions.begin() + i);
        enemies[j].setIndex(j + 1);
    }

    map->generateFlags();
}

void World::denit() {
    delete map;
    time = 0;
    night = false;
    map = NULL;
}

void World::update(float diff) {
    time += diff / DAY_LEN;
    if (getTimeOfDay() > .6 && !night) {
        night = true;
        //player.getMap()->clearFlags();
    } else if (getTimeOfDay() < .6 && night) {
        night = false;
        player.getMap()->generateFlags();
    }
    player.setNight(night);
    for (int i = 0; i < 3; i++) {
        enemies[i].setNight(night);
    }
}

double World::getTime() {
    return time;
}
double World::getTimeOfDay() {
    return std::fmod(time, 1.);
}
int World::getDay() {
    return (int)time;
}

int getDay();

Player& World::getPlayer() {
    return player;
}

Player& World::getEnemy(int i) {
    return enemies[i];
}
