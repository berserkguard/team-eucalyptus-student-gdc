#include "Mob.h"

Mob::Mob(MobType type): type(type) {
    dir = SOUTH;
}

Mob::~Mob() { }

bool Mob::update(float diff) {
    velocity.x *= .75f;
    velocity.y *= .75f;
    translate(std::max(std::min(velocity.x, (float)TERMINAL_VELOCITY), -(float)TERMINAL_VELOCITY),
              std::max(std::min(velocity.y, (float)TERMINAL_VELOCITY), -(float)TERMINAL_VELOCITY));
    return false;
}

sf::Vector2f Mob::getCenterPosition() {
    sf::FloatRect& bb = getCollisionBox();
    return sf::Vector2f(bb.left + bb.width / 2, bb.top + bb.height / 2);
}

void Mob::move(int dir, float speed) {
    switch(dir) {
        case NORTH:     moveRaw(0    , -speed); break;
        case NORTHEAST: moveRaw(speed, -speed); break;
        case EAST:      moveRaw(speed,  0);     break;
        case SOUTHEAST: moveRaw(speed,  speed); break;
        case SOUTH:     moveRaw(0    ,  speed); break;
        case SOUTHWEST: moveRaw(-speed, speed); break;
        case WEST:      moveRaw(-speed, 0);     break;
        case NORTHWEST: moveRaw(-speed, -speed); break;
        default: break;
    }
}

bool Mob::shouldCollide() {
    return true;
}

void Mob::moveAngle(double angle, float speed) {
    moveRaw(std::cos(angle) * speed, std::sin(angle) * speed);
}

MobType Mob::getType() {
    return type;
}

const sf::Vector2f& Mob::getVelocity() {
    return velocity;
}
void Mob::setVelocity(float x, float y) {
    velocity.x = x;
    velocity.y = y;
}
void Mob::setVelocity(sf::Vector2f vel) {
    velocity = vel;
}

void Mob::moveRaw(float x, float y) {
    velocity.x += x;
    velocity.y += y;
}
