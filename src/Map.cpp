#include "Map.h"

#include "tinyxml2/tinyxml2.h"
#include "base64/base64.h"

using namespace tinyxml2;

#define AMMO_DELAY 10

Map::Map(String name): name(name), flags() {
    tiles = NULL;
    flagtex.loadFromFile("assets/flag.png");
    slippery = dangerous = false;
    difficulty = 1;
}
Map::Map(String name, sf::Vector2i size): name(name), size(size) {
    tiles = new Tile[size.x * size.y];
    flagtex.loadFromFile("assets/flag.png");
    slippery = dangerous = false;
    difficulty = 1;
}

Map::~Map() {
    delete[] tiles;
    for (auto iter = entities.begin(); iter != entities.end(); ++iter) {
        delete *iter;
    }
}

String Map::getName() {
    return name;
}

void Map::addEntity(Entity* ent) {
    entities.insert(ent);
}
std::set<Entity*>& Map::getEntities() {
    return entities;
}

void Map::addMob(Mob* mob) {
    mobs.insert(mob);
}
const std::set<Mob*>& Map::getMobs() {
    return mobs;
}

const sf::Vector2i& Map::getSize() {
    return size;
}

Tile& Map::getTile(int x, int y) {
    return tiles[x + y * size.x];
}

Tile& Map::getTile(sf::Vector2i pos) {
    return tiles[pos.x + pos.y * size.x];
}

Tile& Map::getTile(int i) {
    return tiles[i];
}

void Map::update(float diff) {
    // Iterate over all the mobs
    for (auto iter = mobs.begin(); iter != mobs.end(); ++iter) {
        Mob* mob = *iter;
        mob->update(diff);
        if (mob->shouldCollide()) checkWallCollision(mob);
    }

    // Iterate over all the entities
    for (auto iter = entities.begin(); iter != entities.end(); ) {
        Entity* ent = *iter;
        if (ent->update(diff)) {
            entities.erase(iter++);
        } else {
            ++iter;
        }
    }

    if (Rand::f(0, AMMO_DELAY) < diff) {
        int i = Rand::i(0, startingPositions.size());
        Tile& tile = getTile(startingPositions[i]);
        tile.setAmmunition(true);
        ammos.insert(startingPositions[i]);
    }
}

bool Map::checkWallCollision(Mob* mob) {
    // collision
    sf::FloatRect& bb = mob->getCollisionBox();
    sf::Vector2i pos1(bb.left / TILE_SIZE, bb.top / TILE_SIZE);
    sf::Vector2i pos2((bb.left + bb.width) / TILE_SIZE, (bb.top + bb.height) / TILE_SIZE);

    bool collision = false;
    // for west walls
    for (int x = pos1.x + 1; x <= pos2.x; x++) {
        if (x < 0 || x >= size.x) continue;
        bool began = false;
        sf::Vector2f begin, end;
        for (int y = pos1.y; y <= pos2.y; y++) {
            Tile& tile = getTile(x, y);
            if (tile.hasWestWall()) {
                if (!began) {
                    begin = sf::Vector2f(x * TILE_SIZE, y * TILE_SIZE);
                    end = sf::Vector2f(x * TILE_SIZE, (y + 1) * TILE_SIZE);
                    began = true;
                } else {
                    end = sf::Vector2f(x * TILE_SIZE, (y + 1) * TILE_SIZE);
                }
            }
        }
        if (began) {
            handleCollision(mob, begin, end);
            collision = true;
            break;
        }
    }

    // for north walls
    for (int y = pos1.y + 1; y <= pos2.y; y++) {
        if (y < 0 || y >= size.y) continue;
        bool began = false;
        sf::Vector2f begin, end;
        for (int x = pos1.x; x <= pos2.x; x++) {
            Tile& tile = getTile(x, y);
            if (tile.hasNorthWall()) {
                if (!began) {
                    begin = sf::Vector2f(x * TILE_SIZE, y * TILE_SIZE);
                    end = sf::Vector2f((x + 1) * TILE_SIZE, y * TILE_SIZE);
                    began = true;
                } else {
                    end = sf::Vector2f((x + 1) * TILE_SIZE, y * TILE_SIZE);
                }
            }
        }
        if (began) {
            handleCollision(mob, begin, end);
            collision = true;
            break;
        }
    }

    return collision;;
}

void Map::handleCollision(Mob* mob, sf::Vector2f wallBegin, sf::Vector2f wallEnd) {
    sf::FloatRect& bb = mob->getCollisionBox();
    float left = wallEnd.x - bb.left;
    float right = bb.left + bb.width - wallBegin.x;
    float above = wallEnd.y - bb.top;
    float below = bb.top + bb.height - wallBegin.y;
    sf::Vector2f v = mob->getVelocity();
    if (wallBegin.x == wallEnd.x) {
        // vertical wall
        if (left > right) {
            if (right > below - 2) {
                mob->translate(0, -below);
                mob->setVelocity(v.x, 0);
            } else if (right > above - 2) {
                mob->translate(0, above);
                mob->setVelocity(v.x, 0);
            } else {
                mob->translate(-right, 0);
                mob->setVelocity(0, v.y);
            }
        } else {
            if (left > below - 2) {
                mob->translate(0, -below);
                mob->setVelocity(v.x, 0);
            } else if (left > above - 2) {
                mob->translate(0, above);
                mob->setVelocity(v.x, 0);
            } else {
                mob->translate(left, 0);
                mob->setVelocity(0, v.y);
            }
        }
    } else if (wallBegin.y == wallEnd.y) {
        // horizontal wall
        if (above > below) {
            if (below > right - 2) {
                mob->translate(-right, 0);
                mob->setVelocity(0, v.y);
            } else if (below > left - 2) {
                mob->translate(left, 0);
                mob->setVelocity(0, v.y);
            } else {
                mob->translate(0, -below);
                mob->setVelocity(v.x, 0);
            }
        } else {
            if (above > right - 2) {
                mob->translate(-right, 0);
                mob->setVelocity(0, v.y);
            } else if (above > left - 2) {
                mob->translate(left, 0);
                mob->setVelocity(0, v.y);
            } else {
                mob->translate(0, above);
                mob->setVelocity(v.x, 0);
            }
        }
    } else {
        // diagonals wall (IMPOSSIBLE)
    }
}

// takes the data from a tiled layer, converts from base 64 then uncompresses
unsigned* dedatifyText(XMLElement* element, int size) {
    String s = element->FirstChildElement()->GetText();
    s.erase(remove_if(s.begin(), s.end(), isspace)); //trim
    String text = base64_decode(s); //from base 64

    //char* outc = new char[text.size() + 1];
    //strcpy(outc, text.c_str());

    // uncompress
    //char* outc = new char[size * 4];
    //uLongf outputSize = size * 4;
    //uncompress((Bytef*)outc, &outputSize, (Bytef*)text.c_str(), text.size());
    unsigned* out = (unsigned*)text.c_str();

    return out;
}

void Map::loadMapFromTiled(String file) {
    std::cout << "  Loading " << file << std::endl;

    XMLDocument doc; // first load the file
    int success = doc.LoadFile(file.c_str());
    if (success != XML_SUCCESS) success = doc.LoadFile(("assets/" + file).c_str());
    if (success != XML_SUCCESS) {
        std::cout << "   Could not find or load " << file << " or assets/" << file << std::endl;
    }

    XMLElement* mapBase = doc.RootElement();

    size = sf::Vector2i(mapBase->IntAttribute("width"), mapBase->IntAttribute("height"));
    sf::Vector2i tileSize(mapBase->IntAttribute("tilewidth"), mapBase->IntAttribute("tileheight"));
    int numTiles = size.x * size.y;

    delete[] tiles;
    tiles = new Tile[numTiles];

    // parse the tile info
    XMLElement* currentLayer = mapBase->FirstChildElement("layer");

	// Iterate over the chunks, setting the tile types
	for(int layer = 0; layer < NUM_TILE_LAYERS; layer++) {
		unsigned* out = dedatifyText(currentLayer, numTiles);

		for (int i = 0; i < numTiles; i++) {
            Tile& tile = getTile(i);
            tile.setType(layer, out[i] - 1);
		}

		if (currentLayer == mapBase->LastChildElement("layer")) break;
        currentLayer = currentLayer->NextSiblingElement("layer");

        /*if (String(currentLayer->Attribute("name")) == "flags") {
            unsigned* out = dedatifyText(currentLayer, numTiles);
            for (int y = 0; y < size.y; y++) {
                for (int x = 0; x < size.x; x++) {
                    int idx = size.x * y + x;
                    Tile& tile = getTile(x, y);
                    tile.setFlag(!(out[idx] - 65));
                    if (tile.hasFlag()) {
                        flags.insert(sf::Vector2i(x, y));
                    }
                }
            }
            if (currentLayer == mapBase->LastChildElement("layer")) break;
            currentLayer = currentLayer->NextSiblingElement("layer");
        }*/
	}

    // parse the walls
    XMLElement* objectGroup = mapBase->FirstChildElement("objectgroup");
    if (objectGroup && String(objectGroup->Attribute("name")) == "walls") {
        XMLElement* currentObject = objectGroup->FirstChildElement("object");
        while(true) {
            XMLElement* polyline = currentObject->FirstChildElement("polyline");

            sf::Vector2i pos(currentObject->IntAttribute("x"), currentObject->IntAttribute("y"));
            String points = String(polyline->Attribute("points")) + "  ";
            std::istringstream ss(points);
            char a;
            sf::Vector2i prev, next;
            ss >> prev.x >> a >> prev.y;
            while(ss >> next.x >> a >> next.y) {
                sf::Vector2i start = (prev + pos) / tileSize.x;
                sf::Vector2i end = (next + pos) / tileSize.x;
                if (start.x != end.x) {
                    for (int i = std::min(start.x, end.x); i < std::max(start.x, end.x); i++) {
                        getTile(i, start.y).setNorthWall(true);
                    }
                } else {
                    for (int i = std::min(start.y, end.y); i < std::max(start.y, end.y); i++) {
                        getTile(start.x, i).setWestWall(true);
                    }
                }
                prev = next;
            }
            if (currentObject == objectGroup->LastChildElement("object")) break;
            currentObject = currentObject->NextSiblingElement("object");
        }
        objectGroup = objectGroup->NextSiblingElement("objectgroup");
    }
    if (objectGroup && String(objectGroup->Attribute("name")) == "starting points") {
        XMLElement* currentObject = objectGroup->FirstChildElement("object");
        while(true) {
            const char* str = currentObject->Attribute("name");
            if (str && String(str) == "goal") {
                int x = currentObject->IntAttribute("x");
                int y = currentObject->IntAttribute("y");
                goalPosition = sf::Vector2i(x / 64, y / 64);
            } else {
                int x = currentObject->IntAttribute("x");
                int y = currentObject->IntAttribute("y");
                startingPositions.push_back(sf::Vector2i(x / 64 + 1, y / 64));
            }
            if (currentObject == objectGroup->LastChildElement("object")) break;
            currentObject = currentObject->NextSiblingElement("object");
        }
    }
}

void Map::generateFlags() {
    std::set<sf::Vector2i, Dumb> spaces;
    for (int y = 0; y < size.y; y++) {
        for (int x = 0; x < size.x; x++) {
            Tile& tile = getTile(x, y);
            if (tile.isPassable() && !tile.isGoal()) spaces.insert(sf::Vector2i(x, y));
        }
    }

    for (int i = 0; i < NUM_FLAGS; i++) {
        int r = Rand::i(0, spaces.size());
        auto iter = spaces.begin();
        std::advance(iter, r);
        sf::Vector2i space = *iter;
        spaces.erase(iter);
        Flag* flag = new Flag(sf::Vector2f(space * TILE_SIZE), flagtex);
        flags.insert(flag);
    }
}

void Map::clearFlags() {
    for (auto iter = flags.begin(); iter != flags.end(); ++iter) {
        delete *iter;
    }
    flags.clear();
    for (auto iter = scoredFlags.begin(); iter != scoredFlags.end(); ++iter) {
        delete *iter;
    }
    scoredFlags.clear();
}

std::set<Flag*>& Map::getFlags() {
    return flags;
}

void Map::scoreFlag(Flag* flag) {
    scoredFlags.insert(flag);
}

std::vector<sf::Vector2i>& Map::getStartingPositions() {
    return startingPositions;
}
sf::Vector2i& Map::getGoal() {
    return goalPosition;
}

bool Map::isObstructed(sf::Vector2i loc) {
    Tile& tile = getTile(loc);
    int i = tile.getType(0);
    return (i >= 1 && i <= 13);
}

std::set<sf::Vector2i, Dumb>& Map::getAmmos() {
    return ammos;
}

bool Map::isSlippery() {
    return slippery;
}
bool Map::isDangerous() {
    return dangerous;
}
int Map::getDifficulty() {
    return difficulty;
}
