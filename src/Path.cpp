#include <queue>
#include <map>
#include <cmath>
#include <cstddef>
#include "Path.h"

struct Node {
    unsigned int g, h;
    int f;
    sf::Vector2i loc;
    Node* parent;
    Node(Node* p, sf::Vector2i loc, sf::Vector2i dest) : loc(loc), parent(p) {
		refresh(dest);
    }
    unsigned int getG(Node* p) {
        if ((p->loc.x != loc.x) && (p->loc.y != loc.y) /* if diagonal*/ ) {
            return p->g + 14;
        } else {
            return p->g + 10;
        }
    }
    unsigned int getH(sf::Vector2i dest) {
        return std::abs(dest.x - loc.x) + std::abs(dest.y - loc.y);
    }
    void refresh(sf::Vector2i dest) {
        if (parent) {
            g = getG(parent);
        } else {
            g = 0;
        }
        h = getH(dest);
        f = g + h;
    }
};
struct CompareNode: public std::binary_function<Node*, Node*, bool> {
	bool operator()(const Node* lhs, const Node* rhs) const {
		return lhs->f > rhs->f;
	}
};

Path::Path(sf::Vector2i start, sf::Vector2i dest, Grid* grid): dist(-1), len(-1), path(NULL), start(start), dest(dest), grid(grid) { }

Path::~Path() {
    if (path) delete[] path;
}

bool Path::generate() {
    //first set up the grid
    sf::Vector2i gridSize = grid->getSize();
    std::pair<int, Node*> arr[gridSize.x][gridSize.y]; //0 = unvisited, 1 = open, 2 = closed
	std::priority_queue<Node*, std::vector<Node*>, CompareNode> open;
	for (int i = 0; i < gridSize.x; i++) {
		for (int j = 0; j < gridSize.y; j++) {
			if (grid->isObstructed(sf::Vector2i(i, j))) {
				arr[i][j] = std::pair<int, Node*>(2, NULL);
			} else {
				arr[i][j] = std::pair<int, Node*>(0, NULL);
			}
		}
	}
	arr[dest.x][dest.y].first = 0;
	open.push(new Node(NULL, start, dest));
	arr[start.x][start.y] = std::pair<int, Node*>(1, open.top());
	Node* current;

    //next start the loop
	bool done = false;
	bool success = false;
	while(!done) {
        current = open.top();
        open.pop();
		arr[current->loc.x][current->loc.y].first = 2;
        for (int i = 1; i < 9; i++) {

            sf::Vector2i curr = DIRS[i] + current->loc;
            if (curr.x < 0 || curr.x >= gridSize.x || curr.y < 0 || curr.y >= gridSize.y) {
                continue;
            }

			int val = arr[curr.x][curr.y].first;
			if (val == 2) {
				continue;
			} else if (val == 1) {
				Node* n = arr[curr.x][curr.y].second;
				if (n->getG(current) < n->g) {
					Node* temp = n;
					temp->parent = current;
					std::vector<Node*> foonies;
					while (!open.empty()) {
						foonies.push_back(open.top());
						open.pop();
					}
					temp->refresh(dest);
					for (unsigned int i = 0; i < foonies.size(); i++) {
						open.push(foonies[i]);
					}
				}
			} else {
				arr[curr.x][curr.y] = std::pair<int, Node*>(1, new Node(current, curr, dest));
				open.push(arr[curr.x][curr.y].second);
			}
        }
		if (arr[dest.x][dest.y].first == 2) {
			done = true;
			success = true;
		} else if (open.empty()) {
			done = true;
			success = false;
		}
	}
	if (success) {
	    dist = current->g;
	    len = 0;

	    Node* temp = current;
	    while(temp) {

            while (temp->parent && temp->parent->parent) {
                if (walkable(temp->loc, temp->parent->parent->loc)) {
                    temp->parent = temp->parent->parent;
                } else break;
            }

			len++;
			temp = temp->parent;
		}

		path = new sf::Vector2i[len];
		temp = current;
		for (int i = 0; temp; i++) {
		    path[len - i - 1] = temp->loc;
            temp = temp->parent;
		}
		return true;
    }
    len = -1;
    dist = -1;
    return false;
}

int Path::length() {
    return len;
}
float Path::distance() {
    return dist / 10.f;
}
sf::Vector2i Path::at(int i) {
    return path[i];
}

bool Path::walkable(sf::Vector2i from, sf::Vector2i to) {
    //std::pair<int, int> s = std::pair<int, int>((int)(sv.x + .5f), (int)(sv.y + .5f));
    //std::pair<int, int> e = std::pair<int, int>((int)(ev.x + .5f), (int)(ev.y + .5f));
    int dx = abs(to.x - from.x);
    int dy = abs(to.y - from.y);
    int x = from.x;
    int y = from.y;
    int n = 1 + dx + dy;
    int xInc = (to.x > from.x) ? 1 : -1;
    int yInc = (to.y > from.y) ? 1 : -1;
    int error = dx - dy;
    dx *= 2;
    dy *= 2;
    for (; n > 0; --n) {
        if (grid->isObstructed(sf::Vector2i(x, y))) {
            return false;
        } else if (error > 0) {
            x += xInc;
            error -= dy;
        } else {
            y += yInc;
            error += dx;
        }
    }
    return true;
}
