#include "BulletEntity.h"

BulletEntity::BulletEntity(sf::Vector2f location, sf::Vector2f velocity, Player* player, sf::Texture& tex) : Entity(location), velocity(velocity) {
    owner = player;
    startPos = location;
    maxDistance = 750.0f;
    turn = Rand::f(0, 2 * 3.14159);

    sprite.setTexture(tex);
    sprite.setOrigin(sf::Vector2f(tex.getSize()) * .5f);
    sprite.move(sf::Vector2f(tex.getSize()) * .5f);
}

bool BulletEntity::update(float diff){
    Entity::translate(velocity.x * diff, velocity.y * diff);
    sf::Vector2f pos = Entity::getPosition();

    turn += diff * 4;
    sprite.setRotation(turn * 180);

    Map* map = owner->getMap();
    auto& mobs = map->getMobs();
    for (auto iter = mobs.begin(); iter != mobs.end(); ++iter) {
        Mob* mob = *iter;
        if (mob->getType() == PLAYER) {
            Player* player = (Player*)mob;
            player->getCenterPosition();
            sf::Vector2f dist = pos - player->getCenterPosition();
            if (dist.x * dist.x + dist.y * dist.y < 32 * 32) {
                player->getHitByGun(std::atan2(velocity.x, -velocity.y) - 3.14159 / 2, owner);
                if (owner->isNight()) {
                    owner->addScore(2);
                    player->addScore(-1);
                } else {
                    player->addScore(-1);
                }
                return true;
            }
        }
    }

    if (!map->getTile(pos.x / 64, pos.y / 64).isPassable()) {
        return true;
    }

    float xDiff = fabsf(pos.x - startPos.x);
    float yDiff = fabsf(pos.y - startPos.y);
    if(xDiff * xDiff + yDiff * yDiff >= maxDistance * maxDistance) {
        // Destroy the entity
        return true;
    }
    return false;
}
