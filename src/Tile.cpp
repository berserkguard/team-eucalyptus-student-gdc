#include "Tile.h"

Tile::Tile() {
    for (int i = 0; i < NUM_TILE_LAYERS; i++) {
        types[i] = -1;
    }
    northWall = false;
    westWall = false;
    ammunition = false;
}

Tile::~Tile() { }

short Tile::getType(int layer) {
    return types[layer];
}
void Tile::setType(int layer, short type) {
    types[layer] = type;
}

bool Tile::isPassable(){
    switch(types[0]){
    case 0:
    case 16:
    case 17:
    case 18:
    case 19:
    case 24:
    case 25:
    case 26:
    case 27:
        return true;
    }
    return false;
}
bool Tile::isGoal() {
    return (types[0] >= 16 && types[0] < 20) || (types[0] >= 24 && types[0] < 28);
}

void Tile::setNorthWall(bool north) {
    northWall = north;
}
void Tile::setWestWall(bool east) {
    westWall = east;
}
bool Tile::hasNorthWall() {
    return northWall;
}
bool Tile::hasWestWall() {
    return westWall;
}

bool Tile::isLava() {
    return false;
}

void Tile::setAmmunition(bool a) {
    ammunition = a;
}
bool Tile::hasAmmunition() {
    return ammunition;
}
