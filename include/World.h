#ifndef WORLD_H
#define WORLD_H

#include "main.h"
#include "Player.h"

#define DAY_LEN 60

class World {
    public:
        World();
        virtual ~World();

        void init();
        void denit();
        void update(float diff);

        double getTime();
        double getTimeOfDay();
        int getDay();

        void addMap(Map* map);
        Map* getMap(String mapName);

        Player& getPlayer();
        Player& getEnemy(int i);
    private:
        SoundBuffers soundBuffers;

        Player player;
        Player enemies[3];

        double time; // in days
        bool night;

        Map* map;
};

#endif // WORLD_H
