/*
 *  Copyright 2013 Luke Puchner-Hardman
 *
 *  This file is part of Hoarder.
 *  Hoarder is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hoarder is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Hoarder.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RANDOM_H
#define RANDOM_H

#include <random>

class Rand {
    public:
        /// Returns an integer in the range [0, high)
        static unsigned simple(unsigned high);
        /// Returns an integer in the range [low, high)
        static int i(int low, int high);
        /// Returns a float in the range [low, high)
        static float f(float low = 0.f, float high = 1.f);
        /// Returns a double in the range [low, high)
        static double d(double low = 0., double high = 1.);

        /// Randomly picks from a normal distribution.
        static double norm(double mean = 0., double standardDeviation = 1.);
        /// Randomly picks from a log-normal distribution.
        static double lognorm(double mean = 0., double standardDeviation = 1.);
        /// Randomly picks from an exponencial distribution.
        static double exp(double lambda = 1.);

        /// Rolls 'num' dice with 'sides' sides and returns the sum.
        static int dice(int sides, int num = 1);
        /// Given the same parameters, this will return the same result each time [low, high). The difference between 'low' and 'high' is limited to the range of a short.
        static int consistent(int low, int high, int a, int b = 1, int c = 1);
    private:
        Rand();

        static std::mt19937 gen;
        static const int RANDA, RANDB, RANDC, RANDM, RANDN;
};

#endif // RANDOM_H
