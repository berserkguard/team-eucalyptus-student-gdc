/// A utility interface used by certain tools.

#ifndef GRID_H
#define GRID_H

#include "main.h"

class Grid {
    public:
        virtual const sf::Vector2i& getSize() = 0;

        ///Should return whether the given entity take a path though this space.
        virtual bool isObstructed(sf::Vector2i loc) = 0;
};

#endif // GRID_H
