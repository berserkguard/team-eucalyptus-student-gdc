#ifndef MOB_H
#define MOB_H

#include "Entity.h"

enum Direction{NONE, NORTH = 1, EAST = 1 << 2, SOUTH = 1 << 3, WEST = 1 << 4, NORTHEAST = NORTH | EAST, NORTHWEST = NORTH | WEST, SOUTHEAST = SOUTH | EAST, SOUTHWEST  = SOUTH | WEST};
enum MobType{MT_NONE, PLAYER};

#define TERMINAL_VELOCITY 8

class Mob: public Entity {
    public:
        Mob(MobType type);
        virtual ~Mob();

        virtual bool update(float diff);

        sf::Vector2f getCenterPosition();

        const sf::Vector2f& getVelocity();
        void setVelocity(sf::Vector2f vel);
        void setVelocity(float x, float y);

        MobType getType();

        virtual bool shouldCollide();

        /// dir should be a Direction
        void move(int dir, float speed);
        void moveAngle(double angle, float speed);
    private:
        void moveRaw(float x, float y);

        sf::Vector2f velocity;
        Direction dir;

        MobType type;

        //stats, etc
};

#endif // MOB_H
