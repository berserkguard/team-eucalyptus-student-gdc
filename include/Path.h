///You can have paths with this.

#ifndef PATH_H
#define PATH_H

#include "Grid.h"

const sf::Vector2i DIRS[10] = {sf::Vector2i(0, 0), sf::Vector2i(-1, 1), sf::Vector2i(0, 1), sf::Vector2i(1, 1), sf::Vector2i(-1, 0), sf::Vector2i(0, 0), sf::Vector2i(1, 0), sf::Vector2i(-1, -1), sf::Vector2i(0, -1), sf::Vector2i(1, -1)};

class Path {
    public:
        /// Defines a path.
        /**
         * grid is where the path is
         * mob is who the path is for and will be passed to grid->isObstructed
         */
        Path(sf::Vector2i start, sf::Vector2i dest, Grid* grid);
        ~Path();

        /// Generates a path with the A*.
        bool generate();
        /// The number of nodes on the path minus one. Returns -1 if generate() has not been called yet or the pathing failed.
        int length();
        /// The coordines of the next node on the path.
        sf::Vector2i at(int i);
        /// Similar to length(), except diagonals count as 1.4 instead of 1.
        float distance();
    protected:
    private:
        int getH(int x, int y);
        bool walkable(sf::Vector2i from, sf::Vector2i to);

        int dist;
        int len;
        sf::Vector2i* path;
        sf::Vector2i start, dest;
        Grid* grid;
};

#endif // PATH_H
