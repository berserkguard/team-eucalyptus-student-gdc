#ifndef BULLET_ENTITY_H
#define BULLET_ENTITY_H

#include "Entity.h"
#include "Player.h"
#include <SFML/Graphics.hpp>

class BulletEntity : public Entity {
public:
    BulletEntity(sf::Vector2f location, sf::Vector2f velocity, Player* player, sf::Texture& tex);

    virtual bool update(float diff);

private:
    Player* owner;
    sf::Vector2f startPos;
    sf::Vector2f velocity;
    float maxDistance;

    float turn;
};

#endif // BULLET_ENTITY_H
