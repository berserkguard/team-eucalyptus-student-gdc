#ifndef TILE_H
#define TILE_H

#include "main.h"

#define NUM_TILE_LAYERS 3

class Tile {
    public:
        Tile();
        ~Tile();

        short getType(int layer);
        void setType(int layer, short type);

        void setAmmunition(bool a);
        bool hasAmmunition();

        void setNorthWall(bool north);
        void setWestWall(bool west);
        bool hasNorthWall();
        bool hasWestWall();

        bool isLava();

        bool isPassable();
        bool isGoal();
    private:
        short types[NUM_TILE_LAYERS];
        bool northWall, westWall;
        bool ammunition;
};

#endif // TILE_H
