#ifndef PLAYER_H
#define PLAYER_H

#include <SFML/Audio.hpp>

#include "Map.h"
#include "Mob.h"

#define FLAG_POINTS 100
#define SHOT_POINTS 1

const sf::Vector2i startingPositions[] = {sf::Vector2i(34, 34), sf::Vector2i(50, 24) ,sf::Vector2i(34, 16), sf::Vector2i(12, 34)};

class Player;

enum AIState{WAITING, GETTING_FLAG, RETURNING, CHASING, HUNTING, FLEEING};
struct AI {
    AIState state;
    sf::Vector2i currentTarget;
    Path* path;
    Player* nearFlagged;
    int pointOnPath;
    float cooldown;
    float choiceCooldown;
};

struct SoundBuffers {
    sf::SoundBuffer shoot[3], hit, pickup, drop, score;
    SoundBuffers() {
        std::cout << "happens" << std::endl;
        shoot[0].loadFromFile("assets/sounds/shoot1.wav");
        shoot[1].loadFromFile("assets/sounds/shoot2.wav");
        shoot[2].loadFromFile("assets/sounds/shoot3.wav");
        hit.loadFromFile("assets/sounds/hit.wav");
        pickup.loadFromFile("assets/sounds/pickup.wav");
        drop.loadFromFile("assets/sounds/drop.wav");
        score.loadFromFile("assets/sounds/score.wav");
        std::cout << "happens" << std::endl;
    }
};

struct Sounds {
    sf::Sound shoot[3], hit, pickup, drop, score;
};

class Player: public Mob {
    public:
        Player();
        virtual ~Player();

        virtual bool update(float diff);

        void initSounds(SoundBuffers& soundBuffers);

        /// Returns the map the player is currently in.
        Map* getMap();
        /// Moves the player to a new map.
        void setMap(Map* map);

        int getScore();
        void addScore(int score);

        void fireGun();
        void getHitByGun(double angle);

        void pickUpFlag();
        bool hasFlag();
        bool shouldCollide();

        void setIndex(int index);
        int getIndex();

        void setAngle(double angle);
        double getAngle();

        float getStun();

        bool isNight();
        void setNight(int night);
    private:
        //in tiles
        void goToward(sf::Vector2i where, float diff);
        void followPath(float diff);
        void rethink();
        Player* getNearFlagPlayer(bool forReal = true);

        Map* currentMap;
        sf::Texture bullettex;

        bool flag;
        int index;
        int score;
        double angle;

        bool night;

        float stun;
        double stunAngle;

        AI ai;
        Sounds sounds;
};

#endif // PLAYER_H
