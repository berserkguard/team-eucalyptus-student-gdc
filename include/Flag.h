#ifndef FLAG_H
#define FLAG_H

#include "main.h"
#include "Entity.h"

class Flag: public Entity {
    public:
        Flag(sf::Vector2f location, sf::Texture& tex);
        virtual ~Flag();

        bool update(float diff);

        int depoint(float diff);

        sf::Vector2f getCenterPosition();

        void hold();
        void drop();
        void score();
        bool isScored();
        bool isHeld();
    private:
        bool scored;
        bool held;

        int points;
        float next;
};

#endif // FLAG_H
