#ifndef RENDERER_H
#define RENDERER_H

#include "main.h"
#include "Game.h"

#define MTILE_SIZE 4

class Renderer {
    public:
        Renderer();
        virtual ~Renderer();

        void init();
        void render(sf::RenderWindow& window, Game& game);
        void renderNext(sf::RenderWindow& window, Game& game);
    private:
        sf::Texture tileset;
        sf::Texture minitileset;
        sf::Texture ammotex, ammohave, ammoempty;

        sf::Texture playerGun[4];
        sf::Texture playerFlag[4];
        sf::Color playerColors[4];
};

#endif // RENDERER_H
