#include <vector>
#include <map>
#include <set>
#include <string>
#include <iostream>
#include <sstream>
#include <math.h>

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Audio.hpp>

#define WINDOW_WIDTH 1024
#define WINDOW_HEIGHT 768

typedef std::string String;

#define TILE_SIZE 64
