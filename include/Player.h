#ifndef PLAYER_H
#define PLAYER_H

#include "Map.h"
#include "Mob.h"

#define FLAG_POINTS 60

class Player;

enum AIState{WAITING, GETTING_FLAG, RETURNING, CHASING, HUNTING, FLEEING, GETTING_AMMO};
struct AI {
    AIState state;
    sf::Vector2f currentTarget;
    Flag* targetFlag;
    Path* path;
    Player* nearFlagged;
    int pointOnPath;
    float cooldown;
    float choiceCooldown;
};

struct SoundBuffers {
    sf::SoundBuffer shoot[3], hit, pickup, drop, score, ammo;
    SoundBuffers() {
        shoot[0].loadFromFile("assets/sounds/shoot1.wav");
        shoot[1].loadFromFile("assets/sounds/shoot2.wav");
        shoot[2].loadFromFile("assets/sounds/shoot3.wav");
        hit.loadFromFile("assets/sounds/hit.wav");
        pickup.loadFromFile("assets/sounds/pickup.wav");
        drop.loadFromFile("assets/sounds/drop.wav");
        score.loadFromFile("assets/sounds/score.wav");
        ammo.loadFromFile("assets/sounds/ammo.wav");
    }
};

struct Sounds {
    sf::Sound shoot[3], hit, pickup, drop, score, ammo;
};

class Player: public Mob {
    public:
        Player();
        virtual ~Player();

        void reset();
        bool update(float diff);

        void initSounds(SoundBuffers& soundBuffers);

        /// Returns the map the player is currently in.
        Map* getMap();
        /// Moves the player to a new map.
        void setMap(Map* map);

        int getScore();
        void addScore(int score);

        void fireGun();
        void getHitByGun(double angle, Player* who);

        bool pickUpFlag();
        bool hasFlag();
        bool shouldCollide();

        void setIndex(int index);
        int getIndex();

        void setAngle(double angle);
        double getAngle();

        float getStun();

        bool isNight();
        void setNight(int night);

        int getAmmunition();

        bool getDeadInLava();
    private:
        void goToward(sf::Vector2f where, float diff);
        void followPath(float diff);
        void rethink();
        Player* getNearFlagPlayer(bool forReal = true);
        float getVolume();

        Map* currentMap;
        sf::Texture bullettex;

        Flag* flag;
        int index;
        int score;
        double angle;

        bool night;

        float stun;
        double stunAngle;

        bool deadInLava;

        AI ai;
        Sounds sounds;

        float angry;

        int ammunition;
};

#endif // PLAYER_H
