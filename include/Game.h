#ifndef GAME_H
#define GAME_H

#include "World.h"

enum GameState {NO_STATE, IN_GAME, MENU};

class Game {
    public:
        Game();
        virtual ~Game();

        /// Inits game.
        bool init();

        /// Returns the current game state.
        GameState getState();

        /// Returns the game's world.
        World& getWorld();
    private:
        GameState state;
        World world;
};

#endif // GAME_H
