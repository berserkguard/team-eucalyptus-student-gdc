#ifndef ENTITY_H
#define ENTITY_H

#include <SFML/Graphics.hpp>

#include "main.h"

/// An entity has a sprite and a collision box.
class Entity {
    public:
        Entity();
        Entity(sf::Vector2f position);
        Entity(sf::FloatRect collisionBox);
        Entity(sf::Sprite sprite);
        virtual ~Entity();

        /// Called every frame.
        virtual bool update(float diff) = 0;

        sf::Sprite& getSprite();
        sf::FloatRect& getCollisionBox();

        void render(sf::RenderWindow& window);

        void setPosition(sf::Vector2f pos);
        void setPosition(float x, float y);
        const sf::Vector2f getPosition();

        void translate(sf::Vector2f disp);
        void translate(float x, float y);
    protected:
        sf::Sprite sprite;
        sf::FloatRect bounds;
};

#endif // ENTITY_H
