#ifndef MAP_H
#define MAP_H

#include "Tile.h"
#include "Mob.h"
#include "Entity.h"
#include "Path.h"
#include "Rand.h"
#include "Flag.h"

#define NUM_FLAGS 12

struct Dumb {
    bool operator()(const sf::Vector2i& left, const sf::Vector2i& right) {
        if (left.x < right.x) return true;
        else if (left.x > right.x) return false;
        else return left.y < right.y;
    }
};
class Map: public Grid {
    public:
        Map(String name);
        Map(String name, sf::Vector2i size);
        virtual ~Map();

        /// Tells the Map to pass the time.
        void update(float diff);

        /// Returns the name of the Map.
        String getName();

        /// Adds an entity to the map.
        void addEntity(Entity* ent);
        /// Removes an entity from the map.
        //void removeEntity(Entity* ent);
        /// Returns all the Entities in the Map.
        std::set<Entity*>& getEntities();

        void generateFlags();
        void clearFlags();

        /// Adds a Mob to the map.
        void addMob(Mob* mob);
        /// Returns all the Mobs in the Map.
        const std::set<Mob*>& getMobs();

        const sf::Vector2i& getSize();

        bool isObstructed(sf::Vector2i loc);

        /// Returns the Tile at that position.
        Tile& getTile(int x, int y);
        Tile& getTile(sf::Vector2i pos);
        Tile& getTile(int i);

        std::set<Flag*>& getFlags();
        void scoreFlag(Flag* flag);

        std::vector<sf::Vector2i>& getStartingPositions();
        sf::Vector2i& getGoal();

        std::set<sf::Vector2i, Dumb>& getAmmos();

        bool isSlippery();
        bool isDangerous();
        int getDifficulty();

        /// Loads a map from a Tiled file.
        void loadMapFromTiled(String file);
    private:
        /// Checks and handles wall collisions for the given mob.
        bool checkWallCollision(Mob* mob);
        /// Handles a collision for the given mob agains the given wall. The wall must be vertical or horizontal, exist within the Mob, and extend positiveways.
        void handleCollision(Mob* mob, sf::Vector2f wallBegin, sf::Vector2f wallEnd);

        String name;

        std::vector<sf::Vector2i> startingPositions;
        sf::Vector2i goalPosition;

        sf::Vector2i size;
        Tile* tiles;

        sf::Texture flagtex;

        bool slippery, dangerous;
        int difficulty;

        std::set<Mob*> mobs;
        std::set<Flag*> flags;
        std::set<Flag*> scoredFlags;
        std::set<Entity*> entities;
        std::set<sf::Vector2i, Dumb> ammos;
};

#endif // MAP_H
